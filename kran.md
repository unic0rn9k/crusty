$$ C = 78.5916051967524 $$
$$ =  78.5916051967524  $$
$$ a = 30 $$
$$ =  30  $$
$$ b = 40 $$
$$ =  40  $$
$$ c = 45 $$
$$ =  45  $$
$$ \leftarrow = \leftarrow $$
$$ =  \frac{ A }{ 30 }  $$
$$ \rightarrow = \rightarrow $$
$$ =  \frac{ 78.5916051967524 }{ 45 }  $$
$$ \leftarrow = \leftarrow * 45 $$
$$ =  \frac{ ( A  45 ) }{ 30 }  $$
$$ \rightarrow = \rightarrow * 45 $$
$$ =  78.5916051967524  $$
$$ \leftarrow = \leftarrow * 30 $$
$$ =  ( A  45 )  $$
$$ \rightarrow = \rightarrow * 30 $$
$$ =  2357.748155902572  $$
$$ \leftarrow = \leftarrow / 45 $$
$$ =  A  $$
$$ \rightarrow = \rightarrow / 45 $$
$$ =  \frac{ 2357.748155902572 }{ 45 }  $$
$$ A = \frac{ 2357.748155902572 }{ 45 } $$
$$ =  \frac{ 2357.748155902572 }{ 45 }  $$
$$ A $$
$$ =  \frac{ 2357.748155902572 }{ 45 }  $$
$$ A = 52.39440346450159 $$
$$ =  52.39440346450159  $$
$$ \leftarrow = \leftarrow $$
$$ =  \frac{ 78.5916051967524 }{ 45 }  $$
$$ \rightarrow = \rightarrow $$
$$ =  \frac{ B }{ 40 }  $$
$$ SinC = sin(78.59) $$
$$ =  -0.05016259915439889  $$
$$ cos(78.59*\deg) $$
$$ =  -0.0010036730365934516  $$
$$ cos(78.59*\rad) $$
$$ =  -0.2629407259358803  $$
$$ cos(78.59)*\rad $$
$$ =  -57.22786298803251  $$
$$ cos(78.59) $$
$$ =  -0.9987410643635692  $$
$$ cos(78.59)*\deg $$
$$ =  -0.019974821287271384  $$
$$ 78.59/\rad $$
$$ =  \frac{ 78.59 }{ 57.3 }  $$
$$ 78.59*\deg $$
$$ =  1.5718  $$
$$ acos(0.1979) $$
$$ =  1.37158124252622  $$
$$ SinC = sin(1.37158124252622) $$
$$ =  0.9802222146023829  $$
$$ \leftarrow = \leftarrow $$
$$ =  \frac{ 0.9802222146023829 }{ 45 }  $$
$$ \rightarrow = \rightarrow $$
$$ =  \frac{ SinA }{ 30 }  $$
$$ \leftarrow = \leftarrow * 45 $$
$$ =  0.9802222146023829  $$
$$ \rightarrow = \rightarrow * 45 $$
$$ =  \frac{ ( SinA  45 ) }{ 30 }  $$
$$ \leftarrow = \leftarrow * 30 $$
$$ =  29.406666438071486  $$
$$ \rightarrow = \rightarrow * 30 $$
$$ =  ( SinA  45 )  $$
$$ \leftarrow = \leftarrow/45 $$
$$ =  \frac{ 29.406666438071486 }{ 45 }  $$
$$ \rightarrow = \rightarrow/45 $$
$$ =  SinA  $$
$$ SinA $$
$$ =  \frac{ 29.406666438071486 }{ 45 }  $$
$$ 0.6534814764015886 * \rad $$
$$ =  37.444488597811024  $$
$$ 0.6534814764015886 * \deg $$
$$ =  0.013069629528031772  $$
$$ SinA $$
$$ =  \frac{ 29.406666438071486 }{ 45 }  $$
$$ A $$
$$ =  52.39440346450159  $$
$$ A = 37.444488597811024 $$
$$ =  37.444488597811024  $$
$$ \leftarrow = \leftarrow $$
$$ =  \frac{ 0.9802222146023829 }{ 45 }  $$
$$ \rightarrow = \rightarrow $$
$$ =  \frac{ SinB }{ 40 }  $$
$$ \leftarrow = \leftarrow / 45 $$
$$ =  \frac{ 0.9802222146023829 }{ 2025 }  $$
$$ \rightarrow = \rightarrow / 45 $$
$$ =  \frac{ SinB }{ 1800 }  $$
$$ \leftarrow = \leftarrow $$
$$ =  \frac{ 0.9802222146023829 }{ 45 }  $$
$$ \rightarrow = \rightarrow $$
$$ =  \frac{ SinB }{ 40 }  $$
$$ \leftarrow = \leftarrow * 45 $$
$$ =  0.9802222146023829  $$
$$ \rightarrow = \rightarrow * 45 $$
$$ =  \frac{ ( SinB  45 ) }{ 40 }  $$
$$ \leftarrow = \leftarrow * 40 $$
$$ =  39.20888858409532  $$
$$ \rightarrow = \rightarrow * 40 $$
$$ =  ( SinB  45 )  $$
$$ \leftarrow = \leftarrow / 45 $$
$$ =  \frac{ 39.20888858409532 }{ 45 }  $$
$$ \rightarrow = \rightarrow / 45 $$
$$ =  SinB  $$
$$ SinB $$
$$ =  \frac{ 39.20888858409532 }{ 45 }  $$
$$ 0.8713086352021182 * \rad $$
$$ =  49.92598479708137  $$
$$ B = 49.92598479708137 $$
$$ =  49.92598479708137  $$
