use anyhow::*;
use std::collections::HashMap;
use lazy_static::*;
use rand::*;
use crate::Rule as Op;
use std::ops;

macro_rules! opposing{
    ($($a: ident <=> $b: ident),* $(,)?) => {{
        [$(
            ($a, $b),
            ($b, $a)
        ),*].iter().map(|(a,b)|(*a,*b)).collect()
    }}
}

lazy_static!{
    static ref OPPOSING: HashMap<Op, Op> = opposing![
        Mul <=> Div,
        Add <=> Sub,
    ];
    static ref ERR_EQ: Equation = Unknown("!!ERR!!".to_string());
}

#[derive(Clone, Debug, PartialEq)]
pub enum Equation{
    Value(f64),
    Unknown(String),
    Expr(Box<Equation>, Op, Box<Equation>),
    Vector(Vec<Equation>),
    Function(String, HashMap<String, Equation>),
}


macro_rules! impl_op{
    ($( $({} $fn: ident =>)? $op: ident => $o: tt),* $(,)?) => {
        $($(
            impl ops::$op for Equation {
                type Output = Self;

                fn $fn(self, other: Self) -> Self {
                    Expr(Box::new(self), $op, Box::new(other))
                }
            }
        )?)*

        impl Op{
            pub fn to_str(&self) -> String{ match self {$(
                $op => stringify!($o).to_string(),
                )*
                e => panic!("Cannot format Op: {:?}", e),
            }}
            pub fn from_str(s: &str) -> Self{ match s {$(
                stringify!($o) => $op,
                )*
                e => panic!("{:?} is not an op", e),
            }}
            pub fn f(&self, x: f64, y: f64) -> f64{ #[allow(unused)] match self {
                Pow => x.powf(y),
                Equ => (x == y) as u8 as f64,
                Neq => (x != y) as u8 as f64,
                $($($op => {let $fn:u8; x $o y})?)*
                e => panic!("{:?} is not an operator", e),
            }}
        }
    }
}

use Equation::*;
use Op::{Add, Mul, Sub, Div, Pow, Equ, Neq};

impl_op!{
    {} add => Add => +,
    {} sub => Sub => -,
    {} div => Div => /,
    {} mul => Mul => *,
    Equ => ==,
    Neq => !=,
}

macro_rules! Q{
    ($x: expr, $op: tt, $y: expr) => {{
        Expr(
            Box::new($x),
            Op::from_str(stringify!($op)),
            Box::new($y),
        )
    }};
    (? $x: ident) => {{
        Unknown(stringify!($x).to_string())
    }}
}

impl Equation{

    pub fn merge_scope(&self, namespace: &HashMap<String, Equation>) -> HashMap<String, Equation>{
        match self{
            Function(_, scope) => {
                let mut buffer = namespace.clone();
                scope.iter().for_each(|(k,v)|{buffer.insert(k.clone(), v.reduce(namespace));});
                buffer
            },
            e => panic!("Cannot merge_scope of non function: {:?}", e),
        }
    }

    pub fn each(self, op: Op, eq: Equation) -> Equation{
        match self{
            Value(_)   => Expr(Box::new(eq), op, Box::new(self)),
            Unknown(_) => Expr(Box::new(eq), op, Box::new(self)),
            Expr(x, e, y) => {
                Expr(
                    Box::new(Expr( x, op, Box::new(eq.clone()) )),
                    e,
                    Box::new(Expr( y, op, Box::new(eq) )),
                )
            }
            _ => panic!("FUCK SHIT"),
        }
    }

    pub fn reduce(&self, namespace: &HashMap<String, Equation>) -> Equation{
        //println!("{}", self.print());
        match self{
            Function(name, _) =>
                if let Some(function) = namespace.get(name){
                    function.reduce(&self.merge_scope(namespace))
                }else{self.clone()},

            Vector(v) => Vector(v.iter().map(|e| e.reduce(namespace)).collect()),
            Value(_) => self.clone(),

            Unknown(x) => if let Some(v) = namespace.get(x){
                v.reduce(namespace)
            }else{
                self.clone()
            },

            Expr(x, e, y) => {
                let x = x.reduce(namespace);
                let y = y.reduce(namespace);

                if x == y{
                    match e{
                        Div => return Value(1.),
                        Equ => return Value(1.),
                        Neq => return Value(0.),
                        Sub => return Value(0.),
                        Add => return (x * Value(2.)).reduce(namespace),
                        Mul => return Expr(Box::new(x), Pow, Box::new(Value(2.))).reduce(namespace),
                        Pow => {},
                        _ => {},
                    }
                }

                if x == Value(1.) && *e == Mul{
                    return y
                }
                if y == Value(1.) && (*e == Mul || *e == Div){
                        return x
                }
                if y == Value(0.){
                    return match e{
                        Mul => Value(0.),
                        Div => Value(f64::NAN),
                        _ => x,
                    }
                }
                if x == Value(0.){
                    if *e == Mul
                    || *e == Add{
                        return y
                    }
                }

                if *e == Add{
                    const VAL: Equation = Value(0.);
                    if x == VAL{
                        return y
                    }else
                    if y == VAL{
                        return x
                    }
                }

                match (&x, &y){
                    (Unknown(_), Unknown(_)) => {},
                    (Value(_), Unknown(_))   => {},
                    (Unknown(_), Value(_))   => {},

                    (Value(x), Value(y)) => if *e != Div{return Value(e.f(*x, *y))},
                    (Expr(a1, ae, a2), Expr(b1, be, b2)) => if ae == be && ae != e{ return sym_solve(*e, *ae, (a1,a2), (b1,b2)).reduce(namespace) },

                    _ => {
                        if let Expr(x2, e2, y2) = x.clone(){
                            if let Some(res) = tri_solver(*x2, e2, *y2, *e, y.clone()){
                                return res.reduce(namespace)
                            }
                        }else if let Expr(x2, e2, y2) = y.clone(){
                            if let Some(res) = tri_solver(*x2, e2, *y2, *e, x.clone()){
                                return res.reduce(namespace)
                            }
                        }
                    }
                }
                Expr(Box::new(x.clone()), *e, Box::new(y.clone()))
            }
        }
    }

    pub fn is_unknown(&self) -> bool{
        if let Unknown(_) = self{true}
        else{false}
    }

    pub fn print(&self) -> String{
        match self{
            Value(x)       => format!(" {} ", x),
            Unknown(x)     => format!(" {} ", x),

            Expr(x, e, y)  => match e{
                Mul => format!(" ({}{}{}) ",
                    x.print(),
                    if x.is_unknown() == y.is_unknown(){" \\cdot "}else{""},
                    y.print()
                ),
                Div => format!(" \\frac{{{}}}{{{}}} ", x.print(), y.print()),
                Pow => format!(" {}^{{{}}} ", x.print(), y.print()),
                _ => format!(" ({} {} {}) ", x.print(), e.to_str(), y.print())
            },

            Vector(v) => format!("[ {} ]", v.iter().map(|e| format!("{}, ", e.print())).collect::<String>()),
            Function(name, scope) => format!("{}({})",name, scope.iter().map(|(k,v)| format!("{} ={}",k,v.print())).collect::<String>()),
        }
    }

    pub fn calc(&self, namespace: &HashMap<String, Equation>) -> Option<f64>{
        match self{
            Value(x) => Some(*x),
            Unknown(x) => match namespace.get(x){
                Some(x) => x,
                None => return None
            }.calc(namespace),
            Expr(x, e, y) => {
                let x = match x.calc(namespace){
                    Some(x) => x,
                    None => return None,
                };
                let y = match y.calc(namespace){
                    Some(y) => y,
                    None => return None,
                };
                Some(e.f(x,y))
            }
            Vector(_) => panic!("A vector never contains a singular f64"),
            Function(name, scope) => match namespace.get(name){
                Some(f) => f.calc(&scope.iter().map(|(k,v)| (k.clone(), v.reduce(namespace))).collect()),
                None => return None,
            }
        }
    }

    pub fn unit(self) -> Option<(Equation, String)>{
        if let Expr(x, e, y) = self{match e{
            Mul => {
                if let Unknown(u) = *x{return Some((*y, u))}
                if let Unknown(u) = *y{return Some((*x, u))}
            }
            Div => {
                if let Unknown(u) = *x{return Some(( Value(1.) / *y , u))}
                if let Unknown(u) = *y{return Some(( Value(1.) / *x , u))}
            }
            _ => {}
        }}
        None
    }

    pub fn fuzzed_reduce(&self, namespace: &HashMap<String, Equation>) -> Result<Self>{
        let mut fuzz = HashMap::new();

        let pro = self.reduce(&fuzz);

        for _ in 0..100{
            fuzz = namespace.keys().map(|k| (k.clone(), Value((0.5 - random::<f64>())*100.))).collect();
            if self.calc(&fuzz).unwrap() as f32
            != pro.calc(&fuzz).unwrap() as f32{
                return Err(anyhow!("{} != {}", self.reduce(&fuzz).print(), pro.reduce(&fuzz).print()))
            }
        }

        Ok(self.reduce(namespace))
    }

    pub fn semi_fuzzed_reduce(&self, namespace: &HashMap<String, Equation>) -> Result<Self>{
        let mut fuzz = HashMap::new();

        let pro = self.reduce(&fuzz);

        for _ in 0..100{
            fuzz = namespace.keys().map(|k| (k.clone(), Value((0.5 - random::<f64>())*100.))).collect();
            if self.reduce(&fuzz)
            != pro.reduce(&fuzz){
                return Err(anyhow!("{} != {}", self.reduce(&fuzz).print(), pro.reduce(&fuzz).print()))
            }
        }

        Ok(self.reduce(namespace))
    }
}

fn mul_unit(x: &Equation, y: &Equation) -> Option<(Equation, String)>{
    if let Unknown(u) = x{return Some((y.clone(), u.clone()))}
    if let Unknown(u) = y{return Some((x.clone(), u.clone()))}
    None
}

fn sym_solve(outer: Op, inner: Op, a: (&Equation, &Equation), b: (&Equation, &Equation)) -> Equation{
    let e = ||panic!("Missing sym solver for: ( _ {i:?} _ ) {o:?} ( _ {i:?} _ )", o=outer, i=inner);
    match inner{
        Div => {match outer{
            Add => Q!(Q!( Q!( a.0.clone(), *,  b.1.clone()), +, Q!( a.1.clone(), *,  b.0.clone())), /, Q!(a.1.clone(), *, b.1.clone())),
            Sub => Q!(Q!( Q!( a.0.clone(), *,  b.1.clone()), -, Q!( a.1.clone(), *,  b.0.clone())), /, Q!(a.1.clone(), *, b.1.clone())),
            Mul => Q!(Q!(a.0.clone(), *, b.0.clone()), /, Q!(a.1.clone(), *, b.1.clone())),
            _   => e(),
        }}
        Mul => {match outer{

            Div => {
                let au = mul_unit(a.0,a.1).unwrap();
                let bu = mul_unit(b.0,b.1).unwrap();
                if au.1 == bu.1{
                    return Expr(Box::new(au.0), Div, Box::new(bu.0))
                }
                if au.0 == bu.0{
                    return Expr(Box::new(Unknown(au.1)), Div, Box::new(Unknown(bu.1)))
                }
                panic!("IDK")
            }

            op  => {
                let au = mul_unit(a.0,a.1).unwrap();
                let bu = mul_unit(b.0,b.1).unwrap();
                if au.1 == bu.1{
                    return Expr(Box::new(Expr(Box::new(au.0), op, Box::new(bu.0) )), Mul, Box::new(Unknown(au.1)))
                }
                if au.0 == bu.0{
                    panic!("This might be bad")
                }
                panic!("IDK")
            }
        }}
        _   => e(),
    }
}

fn tri_solver(x: Equation, a: Op, y: Equation, b: Op, z: Equation) -> Option<Equation>{
    Some(match (a, b){
        (Mul, Mul) => {
            let mut v = 1.;
            let mut u = Value(1.);
            let mut c = 0;

            for e in [x,y,z].iter(){
                if let Value(n) = e{
                    v*=n
                }else{
                    u = u * (*e).clone();
                    c += 1
                }
            }
            if c > 1{return None}
            Value(v) * u
        }

        (Div, Mul) => if z == y{x}else{x * z / y},
        (Mul, Div) => if z == x{y}else if z == y{x}else{return None},
        (Div, Div) => x /(y * z),
        (Add, Mul) => x * z.clone() + y * z,
        _ => return None,
    })
}
