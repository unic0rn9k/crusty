#[macro_use]
extern crate lazy_static;
extern crate pest;
#[macro_use]
extern crate pest_derive;

use pest::Parser;
use pest::iterators::{Pair, Pairs};
use pest::prec_climber::*;
use anyhow::*;
use regex::Regex;
use std::collections::HashMap;
use std::io::BufRead;
use std::io::{self, Write};
use std::fs::File;
use std::process::Command;

use crossterm::{
    ExecutableCommand, QueueableCommand,
    terminal, cursor, style::{self, Colorize, Styler}
};

#[derive(Parser)]
#[grammar = "latex.pest"]
struct Calculator;

pub use Rule::*;

mod equation;
use equation::Equation;
use Equation::*;

type NAMESPACE = HashMap<String, Equation>;

lazy_static! {
    static ref PREC_CLIMBER: PrecClimber<Rule> = {
        use Assoc::*;

        PrecClimber::new(vec![
            Operator::new(Equ, Left) | Operator::new(Neq, Left),
            Operator::new(Add, Left) | Operator::new(Sub, Left),
            Operator::new(Mul, Left) | Operator::new(Div, Left),
            Operator::new(Pow, Right)
        ])
    };
    static ref LATEX: Regex = Regex::new(r"^( )*\$\$(?P<latex>.*)\$\$").unwrap();

    static ref PARSE_PAIR: fn(Pair<Rule>, &NAMESPACE) -> Result<Equation> = |pair, namespace| Ok(match pair.as_rule() {
            num         => Equation::Value(pair.as_str().parse::<f64>().unwrap()),
            expr        => eval(pair.into_inner(), &namespace)?,
            latex_fn    => latex_eval(pair.into_inner(), &namespace)?,
            builtin_fn  => latex_eval(pair.into_inner(), &namespace)?,
            vec         => vector_eval(pair.into_inner(), &namespace)?,
            user_fn     => function_eval(pair.into_inner(), namespace)?,
            ident       => Equation::Unknown(pair.as_str().to_string()),
            err         => panic!("Unknown compile pattern \"{:?}\"", err),
        });
    static ref PARSE_EPXR: fn(Equation, Pair<Rule>, Equation) -> Equation = |lhs, op, rhs|
        Equation::Expr(Box::new(lhs), op.as_rule(), Box::new(rhs));

    static ref COMMAND: Regex = Regex::new(r"^( )*!(?P<cmd>[a-zA-Z]*)( )*(?P<args>.*)").unwrap();
    static ref PUT: Regex = Regex::new(r#"^( )*"(?P<var>.*)"( )*from( )*(?P<line>.*)"#).unwrap();
    static ref HAS_PANDOC: bool = Command::new("pandoc").arg("-v").status().is_ok();
}

fn eval(expression: Pairs<Rule>, namespace: &NAMESPACE) -> Result<Equation> {
    PREC_CLIMBER.climb(
        expression,
        |p|PARSE_PAIR(p, namespace),
        |a: anyhow::Result<Equation>,b,c|Ok(PARSE_EPXR(a?,b,c?)),
    )
}

fn eval_or_def(expression: Pairs<Rule>, namespace: &mut NAMESPACE) -> Result<Equation> {
    Ok(PREC_CLIMBER.climb(expression,
        |p|match p.as_rule(){
            def => def_eval(p.into_inner(), &mut *namespace),
            _ => PARSE_PAIR(p, &namespace),
        },
        |a: anyhow::Result<Equation>,b,c|Ok(PARSE_EPXR(a?,b,c?)),
    )?)
}

fn def_eval(mut pair: Pairs<Rule>, namespace: &mut NAMESPACE) -> Result<Equation>{
    let name = pair.next().unwrap().as_str().to_string();
    let e = eval(pair.next().unwrap().into_inner(), &namespace)?.reduce(&namespace);
    (*namespace).insert(name, e.clone());
    Ok(e)
}

fn latex_eval(mut pair: Pairs<Rule>, namespace: &NAMESPACE) -> Result<Equation>{
    let builtin = |pair: &mut Pairs<Rule>| -> Result<f64> {Ok(match PARSE_PAIR(pair.next().unwrap(), namespace)?.calc(namespace){
            Some(ok) => ok,
            None => return Err(anyhow!("Currently builtin functions needs to know value of argument at evaluation")),
        })};

    Ok(match pair.next().unwrap().as_str(){
        "\\frac" => PARSE_PAIR(pair.next().unwrap(), namespace)? / PARSE_PAIR(pair.next().unwrap(), namespace)?,
        "sin" => Value(builtin(&mut pair)?.sin()),
        "cos" => Value(builtin(&mut pair)?.cos()),
        "asin" => Value(builtin(&mut pair)?.asin()),
        "acos" => Value(builtin(&mut pair)?.acos()),
        "\\sqrt" => Value(builtin(&mut pair)?.sqrt()),
        "float" => Value(
            if let Some(ok) = PARSE_PAIR(pair.next().unwrap(), namespace)?.calc(namespace)
            {ok}else{
                return Err(anyhow!("Unable to evaluate to exact value"))
            }
        ),
        err => return Err(anyhow!("Unknown latex function {:?}", err)),
    })
}

fn function_eval(mut pair: Pairs<Rule>, namespace: &NAMESPACE) -> Result<Equation>{
    let f_name = pair.next().unwrap().as_str();

    let mut local_scope = HashMap::new();
    let mut a = pair.next();
    let mut b = pair.next();
    while a.is_some(){
        let id  = a.unwrap().as_str();
        let val = eval(b.unwrap().into_inner(), namespace)?;
        local_scope.insert(id.to_string(), val);

        a = pair.next();
        b = pair.next();
    }

    let f = &namespace.get(f_name);
    Ok(if let Some(f) = f{
        f.reduce(&local_scope)
    }else{
        Equation::Function(f_name.to_string(), local_scope)
    })
}

fn vector_eval(mut pair: Pairs<Rule>, namespace: &NAMESPACE) -> Result<Equation>{
    let mut tmp = vec![];

    let mut element = pair.next();
    while element.is_some(){
        tmp.push(eval(element.unwrap().into_inner(), namespace)?);
        element = pair.next();
    }

    Ok(Equation::Vector(tmp))
}

macro_rules! if_let{
    ($match: ident ($var: ident) = ($($source: expr $(=> $is: ident)? ),*) $do: expr) => {{
        $($(let $is = false;)?)*
        $(
            if let $match($var) = $source{
                $(let $is = true;)?
                $do
            }
        )*
    }}
}

fn main() -> Result<()>{
    let mut namespace: HashMap<String, Equation> = [
        ("\\pi",  Value(std::f64::consts::PI)),
        ("deg", Value(0.02)),
        ("rad", Value(57.3)),
    ].iter().map(|(a,b)|(a.to_string(), b.clone())).collect();

    let stdout = io::stdout();
    let mut stdout = stdout.lock();

    let mut o_buffer: Vec<String> = vec![];
    let mut o_file = "out.md".to_string();
    let mut line_nr = 0;
    let mut depth = 0;
    let mut md_buffer: HashMap<usize, String> = HashMap::new();

    let mut prompt = |n, depth| -> anyhow::Result<()>{
        stdout.write_all(format!(":{} {}{}{}{} ", n,
            if depth == 0{" ".to_string()}else{" ".repeat(depth as usize * 2 as usize)+"|-"}.bold().red(),
            ">".bold().red(),
            ">".bold().magenta(),
            ">".bold().cyan(),
        ).as_bytes())?;

        io::stdout().flush()?;
        Ok(())
    };

    println!("{}", "<| cRUSTy |>".bold());
    prompt(0, depth)?;

    let mut parse_line = |line: &str, line_nr: &mut usize, namespace: &mut NAMESPACE, depth|{
        let save = || -> Result<()>{
            let mut buffer = File::create(&o_file)?;
                for (n, line) in o_buffer.iter().enumerate(){
                    buffer.write(&format!("$$ {}{} $$\n", if n%2 == 1{"= "}else{""}, line).as_bytes()[..])?;
                    buffer.write(&md_buffer.get(&n).unwrap_or(&"".to_string()).as_bytes()[..])?;
                }
                Ok(())
        };
        if let Some(command) = COMMAND.captures(&line){
            match &command["cmd"]{
                "remove" => o_buffer[command["args"].replace(" ","").parse::<usize>()?]="".to_string(),
                "open" => o_file = command["args"].replace(" ","_").to_string(),
                "save" => {
                    save()?;
                }
                "put" => {
                    if let Some(put) = PUT.captures(&command["args"]){
                        namespace.insert(put["var"].to_string(), eval(Calculator::parse(
                            calculation,
                            &o_buffer[put["line"].replace(" ","").parse::<usize>()?],
                        )?, &namespace)?.semi_fuzzed_reduce(&namespace)?);
                    }else{return Err(anyhow!("{:?} is not a put statement", &command["args"]))}
                },
                "exit" => std::process::exit(0),
                "exact" => println!("{}", match eval(Calculator::parse(
                            calculation,
                            &o_buffer[command["args"].parse::<usize>()?],
                        )?, &namespace)?.calc(&namespace){
                            Some(ok) => ok,
                            None => return Err(anyhow!("Unable to calculate value")),
                }),
                "md" => {
                    md_buffer.insert(line_nr.clone()*2-1, "\n".to_string()+&command["args"].to_string()+"\n");
                    *line_nr += 1;
                }
                "solve" => {
                    let mut stmts = command["args"].split("=");
                    namespace.insert("\\leftarrow".to_string(),
                        eval(Calculator::parse(
                            calculation,
                            stmts.next().unwrap(),
                    )?, &namespace)?);

                    namespace.insert("\\rightarrow".to_string(),
                        eval(Calculator::parse(
                            calculation,
                            stmts.next().unwrap(),
                    )?, &namespace)?);
                }
                "export" => {
                    if !*HAS_PANDOC{return Err(anyhow!("Unable to export to pdf: Pandoc binary not found"))}
                    save()?;
                    Command::new("pandoc").arg(o_file.clone()).arg("-o").arg(format!("{}.pdf", o_file)).status()?;
                }

                _ => return Err(anyhow!("Unknown command: {:?}", command)),
            }
            return Ok(())
        }

        let out = eval_or_def(Calculator::parse(
            calculation,
            &line,
        )?, namespace)?.semi_fuzzed_reduce(&namespace)?.print();

        println!(":{} {}{}{}", *line_nr*2+1, " ".repeat(depth * 2),"|   =".bold().red(), out);
        o_buffer.push(line.to_string());
        o_buffer.push(out);

        *line_nr+=1;
        Ok(())
    };

    for line in io::stdin().lock().lines(){
        let line = line?;

        let nm = namespace.clone();
        let match_me = (nm.get("\\leftarrow"),
                        nm.get("\\rightarrow"));

        match match_me{
            (Some(left), Some(right)) =>{
                if line == "!exit" || left == right{
                    namespace.remove("\\leftarrow");
                    namespace.remove("\\rightarrow");
                    prompt(line_nr*2, 0)?;
                    continue;
                }

                if_let!{ Unknown(x) = (left => is_left, right){
                    namespace.remove("\\leftarrow");
                    namespace.remove("\\rightarrow");
                    namespace.insert(x.clone(), if is_left{right.clone()}else{left.clone()});
                    prompt(line_nr*2, 0)?;
                    continue;
                }}

                let mut parse_side = |side|{if let Err(err) = parse_line(
                    &line.replace("...", &format!("\\{}arrow", side)),
                    &mut line_nr,
                    &mut namespace,
                    depth
                ){
                    println!("Error on {}: {}\n", side, format!("{}", err).red().bold())
                }};

                parse_side("left");
                parse_side("right");

                prompt(line_nr*2, depth)?;
            }

        _ => {
                if let Err(err) = parse_line(&line, &mut line_nr, &mut namespace, depth){
                    println!("{}\n", format!("{}", err).red().bold())
                }
                prompt(line_nr*2, depth)?;
            }
        }
        depth = (
            namespace.get("\\leftarrow") .is_some() ||
            namespace.get("\\rightarrow").is_some()
        ) as usize;
    }

    Ok(())
}
